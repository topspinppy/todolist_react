import React, { useState } from 'react'
import Service from '../services/task.service.js'
import { Link } from 'react-router-dom'

export const AddTask = () => {
  const [task, setTask] = useState({
    id: null,
    title: '',
    description: '',
    done: false,

    submitted: false,
  })

  const saveTask = () => {
    let data = {
      title: task.title,
      description: task.description,
      done: task.done,
    }
    console.log(data)
    Service.create(data)
      .then(response => {
        setTask({
          id: response.data.id,
          title: response.data.title,
          description: response.data.description,
          done: response.data.done,

          submitted: true,
        })
        console.log(response.data)
      })
      .catch(error => {
        console.log(error)
      })
  }

  const onSetTask = (e, key) => {
    setTask({
      ...task,
      [key]: e.target.value,
    })
  }
  const newTask = () => {
    setTask({
      id: null,
      title: '',
      description: '',
      done: false,

      submitted: false,
    })
  }

  return (
    <div className="submit-form">
      {task.submitted ? (
        <div className="text-center">
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newTask}>
            Add
          </button>
          <Link to={'/tasks'} className="btn btn-primary">
            View all
          </Link>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={task.title}
              onChange={e => onSetTask(e, 'title')}
              name="title"
            />
          </div>

          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              type="text"
              className="form-control"
              rows="3"
              id="description"
              required
              value={task.description || ''}
              onChange={e => onSetTask(e, 'description')}
              name="description"
            ></textarea>
          </div>

          <button onClick={saveTask} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  )
}
