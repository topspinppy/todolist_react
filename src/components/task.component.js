import React, { useState, useEffect } from 'react'
import Service from '../services/task.service.js'

export const Task = props => {
  const { data, setCustomState } = props
  const [initial, setInitial] = useState(data)

  const onSetTask = (e, key) => {
    setInitial({
      ...initial,
      [key]: e.target.value,
    })
  }
  const deleteTask = (e, id) => {
    setCustomState('loading', true)
    Service.delete(id)
      .then(response => {
        console.log(response.data)
        setCustomState('loading', false)
        setCustomState('opened', false)
      })
      .catch(error => {
        console.log(error)
      })
  }

  const updateTask = data => {
    const { _id, ...taskObj } = data
    Service.update(_id, taskObj)
      .then(response => {
        console.log(response.data)
        setInitial(response.data.data)
      })
      .catch(error => {
        console.log(error)
      })
  }

  useEffect(() => {
    setInitial(data)
  }, [data])

  return (
    <div>
      <div className="edit-form">
        <h4>Single task</h4>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            className="form-control"
            id="title"
            value={initial.title}
            onChange={e => onSetTask(e, 'title')}
            name="title"
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            type="text"
            className="form-control"
            id="description"
            value={initial.description}
            onChange={e => onSetTask(e, 'description')}
            name="description"
          />
        </div>
        <button type="submit" className="badge badge-success mr-2" onClick={() => updateTask(initial)}>
          Update
        </button>
        <button type="submit" className="badge badge-danger" onClick={e => deleteTask(e, initial._id)}>
          Delete
        </button>
      </div>
    </div>
  )
}
