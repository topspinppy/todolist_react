import React, { useState, useEffect } from 'react'
import Service from '../services/task.service.js'
import { Task } from './task.component'

export const TaskList = () => {
  const [initial, setInitial] = useState({
    tasks: [],
    currentTask: null,
    currentIndex: -1,
    serachTitle: '',
    loading: false,
    opened: false,
  })

  const onSetState = (e, key) => {
    setInitial({
      ...initial,
      [key]: e.target.value,
    })
  }

  const searchByTitle = () => {
    Service.findByTitle(initial.serachTitle)
      .then(response => {
        setInitial(prevState => ({
          ...prevState,
          tasks: response.data.data,
        }))

        console.log(response.data)
      })
      .catch(e => {
        console.log(e)
      })
  }

  const retrieveTasks = async () => {
    await Service.getAll()
      .then(response => {
        setInitial(prevState => ({
          ...prevState,
          tasks: response.data.data.allTasks,
        }))
      })
      .catch(error => {
        console.log(error)
      })
  }

  const setCustomState = (prevState, stateKey, stateValue) => {
    setInitial({ ...prevState, [stateKey]: stateValue })
  }

  useEffect(() => {
    retrieveTasks()
  }, [])

  const { tasks, searchTitle, currentTask, currentIndex, opened } = initial

  return (
    <div className="list row">
      <div className="col-md-8">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search by title"
            value={searchTitle}
            onChange={e => onSetState(e, 'searchTitle')}
          />
          <div className="input-group-append">
            <button className="btn btn-outline-secondary" type="button" onClick={() => searchByTitle()}>
              Search
            </button>
          </div>
        </div>
      </div>
      <div className="col-md-6">
        <h4>Task Lists</h4>

        <ul className="list-group">
          {tasks &&
            tasks.map((task, index) => (
              <li
                className={'list-group-item ' + (index === currentIndex ? 'active' : '')}
                onClick={() => {
                  setInitial(prevState => ({
                    ...prevState,
                    opened: true,
                    currentTask: task,
                    currentIndex: index,
                  }))
                }}
                key={index}
              >
                {task.title}
              </li>
            ))}
        </ul>
      </div>
      <div className="col-md-6">
        {opened ? (
          <Task data={currentTask} setCustomState={setCustomState} />
        ) : (
          <div>
            <br />
            <p>Please click on a Task...</p>
          </div>
        )}
      </div>
    </div>
  )
}
